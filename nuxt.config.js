export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'ajax',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/apollo',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  apollo:{
    clientConfigs:{
      default:{
        httpEndpoint: 'https://api-us-east-1.graphcms.com/v2/ckqcr0y1fj8bj01xth71dh9vl/master'
      }
    }
  },
  tailwindcss: {
    config: {
      theme: {
        extend: {    
          spacing: {
        
            '100': '30rem',
            '134': '36rem',
            '140': '40rem',
            '143': '43rem',
            '145': '45rem',
            '146': '46rem',
            '150': '50rem',
            '151': '51rem',
            '152': '52rem',
            '154': '54rem',
            '156': '56rem',
            '160': '60rem',
            '164': '64rem',
            '166': '66rem',
            '168': '68rem',
            '172': '76rem',
            '196': '96rem',
            '204': '104rem',
            '1/2': '50%',
          },
          maxWidth: {
            '12xl': '100rem',
           }
         
        }
      }
    }
  },
  
}
